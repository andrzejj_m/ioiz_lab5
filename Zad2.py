import numpy as np
import tensorly as tl
from nn_fac import ntf
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt
import arls

# Z zadania
I1 = 10
I2 = 20
I3 = 30

J = 5

U1 = np.random.randn(I1, J).clip(min=0)
U2 = np.random.randn(I2, J).clip(min=0)
U3 = np.random.randn(I3, J).clip(min=0)

y1 = tl.tensor(U1)
y2 = tl.tensor(U2)
y3 = tl.tensor(U3)

Y = tl.kruskal_to_tensor((np.ones(5), np.array([y1, y2, y3], dtype=object)))
print('Tensor ok')

factors, errors, toc = ntf.ntf(Y, rank=J, init='random', n_iter_max=35, tol=0, normalize=[False, False, False],
                               verbose=True, return_errors=True)
plt.plot(errors, 'b--', label='ALS')

mse = [mean_squared_error(U1, factors[0]), mean_squared_error(U2, factors[1]), mean_squared_error(U3, factors[2])]
print(mse)


plt.legend()
plt.grid()
plt.show()
