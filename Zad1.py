def warn(*args, **kwargs):
    pass
import warnings
warnings.warn = warn
import numpy as np
from sklearn.decomposition import NMF
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error


# Parametry z zadania
I = 100
T = 1000
J = 10

chart_scale = 300

# Generujemy faktory
A = np.random.randn(I, J).clip(min=0)
X = np.random.randn(J, T).clip(min=0)
Y = A.dot(X) # wychodzi macierz 100 x 1000

# Liczymy MUE (solver mu)
print(f'Liczę MUE ({chart_scale} iteracji)')

resid = []
mue_mse = 0
for i in range(1, chart_scale):
    mue_model = NMF(n_components=J, init='random', random_state=0, solver='mu', max_iter=i, tol=1e-20)
    mue_A = mue_model.fit_transform(Y)
    mue_X = mue_model.components_

    n, m = np.dot(mue_A, mue_X).shape
    resid.append(mue_model.reconstruction_err_ / np.sqrt(n * m))
    mue_mse = mean_squared_error(Y, mue_A.dot(mue_X))
    # print(mue_model.reconstruction_err_)

print(f'MUE MSE: {mue_mse}')
plt.semilogy(range(1, chart_scale), resid, 'b--', label='MUE (solver: "mu")')

# Liczymy HALS (solver cd)
print(f'Liczę HALS ({chart_scale} iteracji)')

resid2 = []
hals_mse = 0
for i in range(1, chart_scale):
    hals_model = NMF(n_components=J, init='random', random_state=0, solver='cd', max_iter=i, tol=1e-20)
    hals_A = hals_model.fit_transform(Y)
    hals_X = hals_model.components_

    n, m = np.dot(hals_A, hals_X).shape
    resid2.append(hals_model.reconstruction_err_ / np.sqrt(n * m))
    hals_mse = mean_squared_error(Y, hals_A.dot(hals_X))
    # print(hals_model.reconstruction_err_)

print(f'HALS MSE: {hals_mse}')
plt.semilogy(range(1, chart_scale), resid2, 'g--', label='HALS (solver: "cd")')

plt.legend()
plt.grid()
plt.show()

print('Done')